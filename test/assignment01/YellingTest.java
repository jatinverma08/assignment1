package assignment01;



import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;





public class YellingTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	//R1 one person is yelling
		@Test
		public void testOnePerson() {
			
			Yelling b = new Yelling();
			String result = b.yell("Peter");
			assertEquals("Peter is yelling", result);
		}
		
		//R2 nobody person is yelling
		@Test
			public void testNobody() {
				
			Yelling b = new Yelling();
			String result = b.yell(null);
			assertEquals("Nobody is yelling", result);
			}

		//R3 UpperCase person is yelling
				@Test
					public void testUpperCase() {
						
					Yelling b = new Yelling();
					String result = b.yell("PETER");
					assertEquals("PETER IS YELLING", result);
					}
}
